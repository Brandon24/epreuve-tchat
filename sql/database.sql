create table tchat_room(
  name varchar(30) PRIMARY KEY
);


create table tchat_messageInRoom(
  id integer PRIMARY KEY AUTO_INCREMENT,
  user VARCHAR(30),
  message varchar(150),
  hourOfMessage TIMESTAMP,
  room varchar(30),

  FOREIGN KEY (room) REFERENCES room(name)
);
