<?php
include('php/connexion.php');
if (!empty($_POST['login']) && !empty($_POST['password']) ) {
  // $query = "select login,password FROM user_member where login = ? and password = ? INNER JOIN user_permission
  //           ON user_member.login = user_permission.member";
  $query = "SELECT login, user_permission.permission FROM user_member LEFT JOIN user_permission ON user_member.login = user_permission.member WHERE login = ? and password = ?";
  $result =  $conn->prepare($query);
  $result->bindParam(1, $_POST['login'],PDO::PARAM_STR);
  $result->bindParam(2, md5($_POST['password']),PDO::PARAM_STR);
  $result->execute();

  if ($result->rowCount() == 0) {
    $_POST['error'] = "Le mot de passe ou l'identifiant n'est pas le bon";
  }else{
    session_start();
    $_SESSION['permission']['admin'] = 0;
    $_SESSION['permission']['tchat'] = 0;
    while ($row = $result->fetch()) {
      $_SESSION['pseudo'] = $row["login"];
      if ($row["permission"] == "admin") {
        $_SESSION['permission']['admin'] = 1;
      }elseif ($row["permission"] == "tchat") {
        $_SESSION['permission']['tchat'] = 1;
      }
    }
    header("Location: room.php");
  }
}

 ?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <title></title>
  </head>
  <body>
      <div class="row justify-content-md-center">
        <aside class="col-sm-4">
          <p>Login form style 1</p>
          <div class="card">
            <article class="card-body">
              <a href="" class="float-right btn btn-outline-primary">Sign up</a>
              <h4 class="card-title mb-4 mt-1">Sign in</h4>
              <form method="post" action="index.php">
                <div class="form-group">
                  <label>Your email</label>
                    <input name="login" class="form-control" placeholder="Login" >
                </div> <!-- form-group// -->
                <div class="form-group">
                  <!-- <a class="float-right" href="#">Forgot?</a> -->
                  <label>Your password</label>
                    <input class="form-control" placeholder="******" type="password" name="password">
                </div> <!-- form-group// -->
                <div class="form-group">
                </div> <!-- form-group// -->
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> Login  </button>
                </div> <!-- form-group// -->
              </form>
            </article>
          </div>
        </div>
      </div>


  </body>
</html>

<script type="text/javascript">

var stateObj = { foo: "bar" };
history.pushState("", "", "#lol");

// window.history.pushState("object or string", "Title", "new url");

</script>
