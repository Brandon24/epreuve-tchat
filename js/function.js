var http = new XMLHttpRequest();

function request(url1,call,params,div){
  http.open('POST', url1, true);
  //Send the proper header information along with the request
  http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

  http.onreadystatechange = function() {//Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200 && typeof call === "function") {
          call(http.responseText,div);
      }
  }

  http.send(params);

}


function deleteAllChilds(div){
  while(div.firstChild){
    div.removeChild(div.firstChild);
  }
}
