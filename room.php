<?php
session_start();
if ($_SESSION['permission']['admin'] != 1 && $_SESSION['permission']['tchat'] != 1) {
  header("Location: index.php");
}
 ?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="js/function.js" charset="utf-8"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row" >
        <div class="col-lg-4 mx-auto" id="container">

        </div>

      </div>
      <?php if ($_SESSION['permission']['admin'] == 1): ?>

      <div class="row mt-5" >
        <div class="col-lg-4 mx-auto">
            <input type="text" value="" placeholder="Nom de la room" id="roomName">
            <button type="button" name="button" onclick="ajout()">Ajouter</button>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 mx-auto">
          <a href="createUser.php">Ajouter un Utilisateur NON MAC</a>
        </div>
      </div>
    <?php endif; ?>

  </div>

  </body>
</html>

<script type="text/javascript">

var divParentToInsert =  document.getElementById('container');
getRoom(null,divParentToInsert);

// On appelle ajout pour ajouter la nouvelle room
function ajout(){
 let nameRoom = document.getElementById('roomName').value;
 let params = "room=" + nameRoom;
 request("php/addRoom.php",getRoom,params,divParentToInsert);
}

// Aprés avoir ajouté la nouvelle room alors on fait une requête pour récupérer toutes les rooms
function getRoom(text,div){
  request("php/allRoom.php",updateRoom,null,div);
}

function updateRoom(text,div){
  deleteAllChilds(div);
  div.innerHTML = text;
}


</script>
