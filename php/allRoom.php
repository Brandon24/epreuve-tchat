<?php
header('Access-Control-Allow-Origin: *');
session_start();
if ($_SESSION['permission']['admin'] != 1 && $_SESSION['permission']['tchat'] != 1) {
  header("Location: index.php");
}
include('connexion.php');

$query = "select name from tchat_room";

$result =  $conn->prepare($query);
$result->execute();
// echo '<div class="col-lg-4 mx-auto">';
echo "<h2>Liste des Salons Disponibles</h2>";
echo '<ul class="list-group ">';

while ($row = $result->fetch()) {
  echo '<li class="list-group-item">';
    echo '<div class="row">';
      echo '<div class="col-8">';
        echo "<a href='roomTchat?room=" . $row["name"] . "'>".$row["name"]."</a>";
      echo '</div>';
      echo '<div class="col-2">';
      $a =$row["name"];
      if ($_SESSION['permission']['admin'] == 1) {
        echo "<a href='php/deleteRoom.php?room=$a'>Suppresion</a>";
      }
        // $lol = "<data value=".$row["name"].">Suppression</data>";
        // echo "<button class='event' type='button' name='button'>$lol</button>";

      echo '</div>';
    echo '</div>';
  echo "</li>";
}
echo "</ul>";
 ?>
