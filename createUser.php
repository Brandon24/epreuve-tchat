<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  </head>
  <body>
    <style media="screen">
    .note
  {
    text-align: center;
    height: 80px;
    background: -webkit-linear-gradient(left, #0072ff, #8811c5);
    color: #fff;
    font-weight: bold;
    line-height: 80px;
  }
  .form-content
  {
    padding: 5%;
    border: 1px solid #ced4da;
    margin-bottom: 2%;
  }
  .form-control{
    border-radius:1.5rem;
  }
  .btnSubmit
  {
    background: #0062cc;
    color: #fff;
  }
    </style>

  <div class="container register-form">
    <a href="room.php">Revenir a la Page Principal</a>

            <div class="form">
                <div class="note">
                    <p>Forms for create a new user.</p>
                </div>

                <div class="form-content">
                    <div class="row justify-content-md-center">
                      <form class="col-6 mx-auto " action="php/addUser.php" method="post">
                        <div class="col-md-6 mx-auto">
                          <div class="form-group">
                              <input type="text" class="form-control"name="login" placeholder="Your Name *" value=""/>
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="password"placeholder="Your Password *" value=""/>
                          </div>
                          <div class="form-group">
                            <input type="submit" class=" btnSubmit form-control" placeholder="Confirm Password *" value="Validation"/>
                          </div>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>


  </body>
</html>
