<?php
session_start();
if ($_SESSION['permission']['admin'] != 1 && $_SESSION['permission']['tchat'] != 1) {
  header("Location: index.php");
}

 ?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="js/function.js" charset="utf-8"></script>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="container-fluid">

      <div class="row">
       <div class=" col-2 mx-auto">
         <h1 class="text-center"><?php echo $_GET["room"]; ?></h1>
         <div class=""id="listMessage">

         </div>
       </div>
      </div>

   <div class="row">
     <div class=" col-2 mx-auto">
       <input type="text" id="msg" value="">
       <button type="button" name="button" id="button">Envoyer</button>
     </div>
   </div>

 </div>
  </body>
</html>

<script type="text/javascript">

var url = 'listMessage.php';
var room = '<?php echo $_GET['room']; ?>';
var pseudo =  "<?php echo $_SESSION['pseudo'] ?>";

var divContainerAllMessage = document.getElementById('listMessage');
var button = document.getElementById('button');
var inputMessageUser = document.getElementById('msg');

var changePosition = true;


setInterval(function(){
  // rafraichissage des messages
  let params = "room="+room;
  request(url,callBack,params,divContainerAllMessage);

},500);

button.onclick = function(event){
  let messageUser = inputMessageUser.value;
  if (messageUser) {
    let params = "message=" + messageUser + "&room=" + room + "&pseudo=" + pseudo;
    request("addMessage.php",null,params,null);
    inputMessageUser.value = "";
    changePosition = true;
  }
}

function callBack(text,div){
  div.innerHTML = text;
  if (changePosition) {
    changePosition = false;
    let allChilds = div.querySelectorAll("p");
    console.log(allChilds.length);
    document.location =  '#' + allChilds[allChilds.length-1].id;
  }

}


</script>
